= Arm Cortex M4 
include::{includedir}/slideshow_default_attr.adoc[]
:email:     tony.j@e-consystems.com 
:date:      {docdate} 
:author:    Tony Lijo Jose 
:revnumber: 1.0 
:revdate:   {docdate} 
:customer:  Internal 
:imagesdir-old: {imagesdir}
:imagesdir: {topdir}/output
:title-logo-image: image::title_logo.png[width=300%,top=1%]
:footer_recto_content: none
:footer-image: image:{imagesdir}/footer_image.png[width=80%]
:logotitle: 
:empty:



include::./sections.adoc[]
